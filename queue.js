let collection = [];

// Write the queue functions below.
// output all elements of the queue
function print(){
    return collection;

};

// add element to the rear
function enqueue(name){
    collection[collection.length] = name;
    return collection
};

// removes element at the front
function dequeue(){
    return collection.shift()
};


// shows element at the front
function front(){
    return collection[0]
};

function size(){
    return collection.length
};

function isEmpty(){
    if(collection.length === 0){
        return true
    }else{
        return false
    }
};


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};